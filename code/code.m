% Load
PATH = './inProcess.jpg';
img = imread(PATH);

[r, c, d] = size(img);
%%
% Part 0
for index = 1:r
    R(index) = 2*(index/r)-1-1/r;
end
for index = 1:c
    C(index) = 2*(index/c)-1-1/c;
end

[X,Y] = meshgrid(C,R);
Z = X + Y*1i;
%%
% Part 1
r1 = 0.2; r2 = 0.9;
[r, c, d] = size(img);

W = Wmapping1(Z, r1, r2);

Wx = real(W);
Wy = imag(W);

Wxmax = max(max(abs(Wx)));
Wymax = max(max(abs(Wy)));

Xnew = floor((Wx/Wxmax + 1) * c/2);
Ynew = floor((Wy/Wymax + 1) * r/2);
%%
% Part 3
rep = 4;

Y = Ynew;
for re = 1:rep-1
    Y = [Y
        (Ynew+re*r)];
end
Ynew = Y;
Xnew = repmat(Xnew,rep,1);
%%
% Part 4
Xnew = (2*(Xnew/c)) - 1;
Ynew = (2*(Ynew/r)) - rep;

Z = Xnew + Ynew*1i;
W = Wmapping3(Z, r1, r2);

Wx = real(W);
Wy = imag(W);

Wxmax = max(max(abs(Wx)));
Wymax = max(max(abs(Wy)));

Xnew = floor((Wx/Wxmax + 1) * c/2);
Ynew = floor((Wy/Wymax + 1) * r*rep/2);
%%
% Part 5
Xnew = (2*(Xnew/c)) - 1;
Ynew = (2*(Ynew/r)) - rep;

Z = Xnew + Ynew*1i;
W = Wmapping4(Z);

Wx = real(W);
Wy = imag(W);

Wxmax = max(max(abs(Wx)));
Wymax = max(max(abs(Wy)));

Xnew = floor((Wx/Wxmax + 1) * c/2);
Ynew = floor((Wy/Wymax + 1) * r*rep/2);
%%
imgNew = uint8(zeros(r,c,3));
for row = 1:rep*r
    for col = 1:c
        imgNew(Ynew(row,col) + 1, Xnew(row,col) + 1, 1:3) = img( mood(row,r)+1, col, 1:3);
    end
end
image(imgNew);
%%
function c = condition(z, r1, r2)
    c = (r1<abs(z) & r2>abs(z));
end

function w = Wmapping1(z, r1, r2)
    w = condition(z,r1,r2) .* log(z/r1);
end

% PART 4 //////////////////////////////////////////////
function w = Wmapping3(z, r1, r2)
    a = atan(log(r2/r1) / (pi));
    w = z * cos(a) * exp(a * 1i);
end

% PART 5 //////////////////////////////////////////////
function w = Wmapping4(z)
    w = exp(3*z);
end

% LAST
function m = mood(a, b)
    m = a - b * floor(a/b);
end
