# Droste
Engineering Mathematics - UT CE - Spring 2019

In this computer assignment we tried to implement a program in purpose Droste mapping with Engineering Mathematics Mapping knowledge.

![Alt text](./readme.PNG?raw=true "Game Environment")

### Reaurements :
 
 - MatLab

### Contributors :

 - Behzad Shayegh

### Test :

To test, put a picture beside code.m and rename it as inProcess.jpg, then run the code and get result. you can also find some samples and results in test directory.
